from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'django_nginx_test_package.views.home', name='home'),
    # url(r'^django_nginx_test_package/', include('django_nginx_test_package.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.home),
    url(r'^step1$', views.step1_handler),
    url(r'^step2$', views.step2_handler),
)
urlpatterns += staticfiles_urlpatterns()
