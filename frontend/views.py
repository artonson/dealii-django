from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext

from subprocess import check_call
import logging
import os

log = logging.getLogger("django.request")

import lib.step1 as step1
import lib.step2 as step2

def home(request):
    return render_to_response("index.html")

def step1_handler(request):
    step1.first_grid()
    check_call(['convert', 'grid-1.eps', 'frontend/static/grid-1.png'])
    step1.second_grid()
    check_call(['convert', 'grid-2.eps', 'frontend/static/grid-2.png'])
    context = {'image1': 'grid-1.png', 'image2': 'grid-2.png'}
    return render_to_response("step1.html", RequestContext(request, context))

def step2_handler(request):
    step2.the_main_method()
    check_call(['./plotscript.sh', 'sparsity_pattern.1', 'frontend/static/sparsity_pattern.1.png'])
    check_call(['./plotscript.sh', 'sparsity_pattern.2', 'frontend/static/sparsity_pattern.2.png'])
    context = {'image1': 'sparsity_pattern.1.png', 'image2': 'sparsity_pattern.2.png'}
    return render_to_response("step2.html", RequestContext(request, context))

