*** Package description

The aim of this package is to provide an example of how to create
and use python bindings for deal.ii library.


*** Installation

1. Install git, nginx, python-pip, swig
    # sudo apt-get install git nginx python-pip
2. Install django
    # sudo pip install django
3. Either download and build from source deal.ii or use built binaries.
4. Go to src/ directory and change makefiles to have
DEAL_II_LIB_PATH, DEAL_II_INCLUDE_PATH and DEAL_II_INCLUDE_B_PATH
point to deal.ii directories as described below in 'Extending this example'.
5. Compile each example using make. Binary files should appear
in lib/ directory.
6. Run the Django development server from within the root directory
    # ./run_frontend.sh

*** Directories

conf                    nginx configuration files
debian                  files required to build the package
lib                     python binary modules and corresponding .py modules
site_project            Django back-end web server
src                     C source code


*** Extending this example

To add another page to the experiments, you should:
1. Create a subdirectory under src/ directory containing the following files:
    *.cc                C source containing experiment computation module
    *.i                 SWIG interface file
    Makefile            file used by GNU make

In the interface file, declare and export the routines to be used
by your python program.

2. Compile the binary library (*.so file) and the python source (*.py)
using GNU make (which will invoke SWIG and g++ to build them).
In order to build against deal.ii shared library, you should have the following
environment variables in the Makefile:
    DEAL_II_LIB_PATH        path to deal.ii shared library (*.so)
    DEAL_II_INCLUDE_PATH    path to deal.ii include files
    DEAL_II_INCLUDE_B_PATH  path to deal.ii bundled include file
Note you also need to set PYTHON_LIB_PATH to point to include files for your
distribution of Python.
At this point you should be able to import the compiled module into
a python script. Remember to set LD_LIBRARY_PATH environment variable
to contain DEAL_II_LIB_PATH.

3. Create an HTML template for the new experiment. Tutorial on writing
templates for Django is at https://docs.djangoproject.com/en/dev/ref/templates/.
Link the template to the homepage.

4. Create a view handler. Tutorial on writing views is at
https://docs.djangoproject.com/en/dev/topics/http/views/.
Note you should import the newly-created python module and call its methods
from the view function.

5. Voila. Navigate in the browser to the newly created page and check it out.


